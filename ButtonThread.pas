//
unit ButtonThread;

interface

uses
  System.Classes, SysUtils, Posix.Systime, System.Generics.Collections;

type
  TDCStateProc = procedure of object;
  TDCStateProc1 = pointer;
  TDCStateDo = TDictionary<string, TDCStateProc>;

  TButtonThread = class(TThread)
  protected
    procedure Execute; override;
    procedure ReadAnsw;
    procedure DC_I_increment;
    procedure DC_PWM_start;
    procedure DC_GasOpen_ResetT;
    procedure DC_I_decrement_ResetT1;
    procedure DC_GasClose;
    procedure DC_PWM_stop;
    procedure checkparams;
    function sendCommand(cmd: String): String;

    procedure OpenGas;
    procedure CloseGas;
    procedure StartPWM;
    procedure StartAC;
    procedure StopPWM;
    procedure SetI(I: integer);
    procedure SetBal(Bal: integer);		//в этом потоке не нужно
    procedure SetFreq(Freq: integer);		//в этом потоке не нужно
    procedure SetIClean(I: integer);		//в этом потоке не нужно
//	procedure Fan(ss: integer);

  var
    FDCStateProc: TDCStateProc;

    property DCState: TDCStateProc read FDCStateProc write FDCStateProc;
    // property StateProc: TDCStateProc;

  var
    DCStateDo: TDCStateDo;
    bufsize: integer;
    answer: string;
    cmdStatus: boolean;
    rcvByte: byte;
    TV_: Timeval;
    TV: Timeval;
    TV1: Timeval;
    TV_I: Timeval;
    tvdiv: integer;
    isPWM: boolean;
    t, t1, t_I: integer;
    Ic, U, isBut, isGas, isGas1: integer;

  I_Min: integer;
  I_Max: integer;
  tCloseGas: integer;    // время, через которое выключается газ при минимальном токе
  tStartPWM: integer;     // время, через которое стартует ШИМ
  tImax: integer;        // время, за которое ток должен достичь максимума
  tImin: integer;        // время, за которое ток должен достичь минимума


    /// settings
    I: integer;
    stDCState: string;
    DCStateDostr: TStringList;
    kImax: Extended;
    kImin: Extended;
    bImax: Extended;
    bImin: Extended;
    isIncrement:boolean;
    isDecrement:boolean;

  end;

  { TDCState = (
    NullState = 'XXXXXXX',                   // 7o
    DC_I_increment = '111110X',         //  111110X
    DC_PWM_start = '011X1XX',           //  011X1XX
    DC_GasOpen_ResetT = 'XX1X0XX',      //  XX1X0XX
    DC_I_decrement_ResetT1 = 'XX01XXX', //  XX01XXX
    DC_GasClose = '0X001X1',            //  0X001X1
    DC_PWM_stop = '1X00XXX'             //  1X00XXX
    ); }

//	1 - isPWM
//	2 - t >= tStartPWM (tPref)
//	3 - isBut (torch)
//	4 - Ic >= 4
//	5 - isGas
//	6 - I >= I_Max
//	7 - t1 > tCloseGas (tPof)
//	8 - I <= I_Min
//	9 - not(not(U) and lift)

			   //'12345678'  // 123456789
const
  stNullState =              'XXXXXXXX'; // 
  stDC_I_increment =         '111110XX'; // 111110XX1
  stDC_PWM_start =           '011X1XXX'; // 011X1XXXX
  stDC_GasOpen_ResetT =      'XX1X0XXX'; // XX1X0XXXX
  stDC_I_decrement_ResetT1 = 'XX01XXXX'; // XX01XXXXX	над флагом 4 подумать, может быть XX01XXX0X или XX0XXXX0X
  stDC_GasClose =            '0X001X1X'; // 0X001X1XX
  stDC_PWM_stop =            '1X0XXXX1'; // 1X0XXXX1X
  // stDC_PWM_stop =            '1X00XXX';            //  1X00XXX
  Ic_Start = 4;

implementation

uses TerminalUnit;

{ TButtonThread }

procedure TButtonThread.DC_I_increment;
var TV_Iabs:integer;
begin
  // t:=0;
  if I < I_Min then
    I:= I_Min;

  if isIncrement = false then
  begin
    t_I:= round((I - bImax) / kImax); // расчет текущего положения на оси в соответствии с током I
    TV_Iabs:= TV_.tv_sec*1000000 + TV_.tv_usec;
    TV_Iabs:= TV_Iabs - t_I;
    TV_I.tv_sec:= TV_Iabs div 1000000;
    TV_I.tv_usec:= TV_Iabs - (TV_I.tv_sec)*1000000;
  end;

  isIncrement:=true;
  isDecrement:=false;

  I:= round(t_I * kImax + bImax);
  if I > I_Max then
    I:= I_Max;

  SetI(I);
end;

procedure TButtonThread.DC_PWM_start;
begin
  StartPWM;
  isPWM:= true;
end;

procedure TButtonThread.DC_GasOpen_ResetT;
begin
//Fan(1);
  OpenGas;
  isGas:= 1;
  t:= 0;
  gettimeofday(TV, nil);
end;

procedure TButtonThread.DC_I_decrement_ResetT1;
var TV_Iabs:integer;
begin
  if I > I_Max then
    I:= I_Max;

  if isDecrement = false then
  begin
    t_I:= round((I - bImin) / kImin); // расчет текущего положения на оси в соответствии с током I
    TV_Iabs:= TV_.tv_sec*1000000 + TV_.tv_usec;
    TV_Iabs:= TV_Iabs - t_I;
    TV_I.tv_sec:= TV_Iabs div 1000000;
    TV_I.tv_usec:= TV_Iabs - (TV_I.tv_sec)*1000000;
  end;

  isIncrement:=false;
  isDecrement:=true;

  I:= round(t_I * kImin + bImin);
  if I < I_Min then
    I:= I_Min;

  SetI(I);
  t1:= 0;
  gettimeofday(TV1, nil);
end;

procedure TButtonThread.DC_GasClose;
begin
//Fan(0);
  CloseGas;
  isGas:= 0;
end;

procedure TButtonThread.DC_PWM_stop;
begin
  StopPWM;
  isPWM:= false;
end;

procedure TButtonThread.Execute;

  procedure parseParams(const str: string; var Ic: integer; var U: integer; var isBut: integer);
  var
    I, spos, sInd: integer;
    tmpstr: string;
    strArray: TStringList;
  begin
    strArray:= TStringList.Create;
    strArray.Delimiter:= ' ';
    strArray.StrictDelimiter:= true;
    strArray.DelimitedText:= str;
    if strArray.Count > 2 then
    begin
      Ic:= strtoint(strArray[0]);
      U:= strtoint(strArray[1]);
      isBut:= strtoint(strArray[2]);
    end
    else
    begin
      Ic:= -1;
      U:= -1;
      isBut:= -1;
    end;
  end;

  procedure getTVdiv;
  begin
    gettimeofday(TV_, nil);
    t:= (TV_.tv_sec * 1000000 + TV_.tv_usec) - (TV.tv_sec * 1000000 + TV.tv_usec);
    t1:= (TV_.tv_sec * 1000000 + TV_.tv_usec) - (TV1.tv_sec * 1000000 + TV1.tv_usec);
    t_I:= (TV_.tv_sec * 1000000 + TV_.tv_usec) - (TV_I.tv_sec * 1000000 + TV_I.tv_usec);
    { if tvdiv < 0 then
      tvdiv:= 1000000 + tvdiv; }
  end;

  function compareState(const stDCState: string; const CalculatedState: string): boolean;
  var
    I: integer;
  begin
    for I:= 0 to length(stDCState) - 1 do
      if (stDCState[I] <> 'X') and (stDCState[I] <> CalculatedState[I]) then
        break;
    if I = length(stDCState) then
      result:= true
    else
      result:= false;
  end;

  procedure DoState(const CalculatedState: string);
  var
    key: string;
    I: integer;
  begin
    // for key in DCStateDo.Keys do
    for I:= 0 to DCStateDostr.Count - 1 do
    begin
      // DCStateDo.TryGetValue(Key, StateProc);
      key:= DCStateDostr[I];
      if (compareState(key, CalculatedState)) then
      begin
        if (key = stDC_I_increment) then          DC_I_increment;
        if (key = stDC_PWM_start) then            DC_PWM_start;
        if (key = stDC_GasOpen_ResetT) then       DC_GasOpen_ResetT;
        if (key = stDC_I_decrement_ResetT1) then  DC_I_decrement_ResetT1;
        if (key = stDC_GasClose) then             DC_GasClose;
        if (key = stDC_PWM_stop) then             DC_PWM_stop;
        // DCStateDo.Items[key];
        //ReadAnsw;
      end;
    end;

  end;

  procedure setstate;
  begin
    DCState:= DC_I_increment;
  end;

begin
  DCStateDostr:= TStringList.Create;
  DCStateDostr.Add(stDC_I_increment);
  DCStateDostr.Add(stDC_PWM_start);
  DCStateDostr.Add(stDC_GasOpen_ResetT);
  DCStateDostr.Add(stDC_I_decrement_ResetT1);
  DCStateDostr.Add(stDC_GasClose);
  DCStateDostr.Add(stDC_PWM_stop);

  isIncrement:=false;
  isDecrement:=false;

 // DoState(stDC_GasOpen_ResetT);

  answer:= '';
  cmdStatus:= false;
  isGas1:= 0;
  isGas:= 0;
  isPWM:= false;

//  dI:= 1;
//  dI1:= 1;
  I:= 5; //I:= I_Min;

  SendCommand('d');                 // инициализация режима TIG DC
  //ReadAnsw;

  SetI(5); //SetI(I);
  //ReadAnsw;
  CloseGas;
//Fan(0);
  //ReadAnsw;
  gettimeofday(TV, nil);
  gettimeofday(TV1, nil);
  gettimeofday(TV_I, nil);

  while not Terminated do
    with form1 do
    begin
      synchronize(checkparams);

      answer:= sendCommand('l');
      //ReadAnsw;
      if (answer<>'read timeout')and(pos('nvalid',answer)<=0) then
      begin
        parseParams(answer, Ic, U, isBut);

        getTVdiv; // считаем t и t1

        stDCState:= stNullState; // вычисляем "слово-состояние"
        if isPWM then          stDCState[0]:= '1'   else stDCState[0]:= '0';
        if t >= tStartPWM then stDCState[1]:= '1'   else stDCState[1]:= 'X'; //   500000
        if isBut > 0 then      stDCState[2]:= '1'   else stDCState[2]:= '0';
        if Ic >= 4 then        stDCState[3]:= '1'   else stDCState[3]:= '0';
        if isGas > 0 then      stDCState[4]:= '1'   else stDCState[4]:= '0';
        if I >= I_Max then     stDCState[5]:= 'X'   else stDCState[5]:= '0'; //   27
        if t1 > tCloseGas then stDCState[6]:= '1'   else stDCState[6]:= 'X'; //   6000000
        if I <= I_Min then     stDCState[7]:= '1'   else stDCState[7]:= 'X'; //   5
//	if not(not(U) and lift) then     stDCState[8]:= '1'   else stDCState[8]:= 'X';

        DoState(stDCState);
      end;
      sleep(10);
//      synchronize(
//        procedure
//        begin
//          form1.stateWord.text:= stDCState;
//        end
//        );
    end;
    sendCommand('q');
    Synchronize(form1.IDC.Disconnect);
end;

procedure TButtonThread.ReadAnsw;
begin
  Synchronize(
    procedure
    begin
      answer:= form1.readanswer;
    end);
  //
end;

//procedure TButtonThread.Fan(ss: integer);
//begin
//    sendCommand('F'+inttostr(ss));
//end;

procedure TButtonThread.SetBal(Bal: integer);
begin
    sendCommand('b'+inttostr(Bal));
end;

procedure TButtonThread.SetFreq(Freq: integer);
begin
  sendCommand('f'+inttostr(Freq));
end;

procedure TButtonThread.SetI(I: integer);
begin
  sendCommand('a'+inttostr(I));
end;

procedure TButtonThread.SetIClean(I: integer);
begin
  sendCommand('c'+inttostr(I));
end;

procedure TButtonThread.StartPWM;
begin
  sendCommand('s');
end;

procedure TButtonThread.StartAC;
begin
  sendCommand('~');
end;

procedure TButtonThread.StopPWM;
begin
  sendCommand('e');
end;

procedure TButtonThread.checkparams;
begin
  if ChangeParamsFlag then
  with form1 do
  begin
    I_Min:= round(tb_Imin.Value);
    I_Max:= round(tb_Imax.Value);
    tCloseGas:= round(tb_tCloseGas.Value*1000000);    // время, через которое выключается газ при минимальном токе
    tStartPWM:= round(tb_tStartPWM.Value*1000000);     // время, через которое стартует ШИМ
    tImax:= round(tb_tImax.Value*1000000);        // время, за которое ток должен достичь максимума
    tImin:= round(tb_tIMin.Value*1000000);        // время, за которое ток должен достичь минимума
    kImax:= (I_max - I_min) / (tImax);
    kImin:= (I_min - I_max) / (tImin);
    bImax:= I_min - kImax*0;
    bImin:= I_max - kImin*0;
    ChangeParamsFlag:=false;
  end;
end;

procedure TButtonThread.CloseGas;
begin
  sendCommand('r');
end;

procedure TButtonThread.OpenGas;
begin
  sendCommand('g');
end;

function TButtonThread.sendCommand(cmd: String): String;
var res:string;
begin
  synchronize(
  procedure
  begin
    res:= form1.sendCommand(cmd);
  end
  );
  result:= res;
end;

end.
